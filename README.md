# Daily Phaser

電子書籍 "実録 スターデストロイヤーゲーム開発日記" で開発されたゲームの公開リポジトリです。

書籍出版日：2020年9月12日　技術書典9

https://techbookfest.org/event/tbf09

## スターデストロイヤーゲーム

ver 1.0.1

書籍の後半で開発されたゲームです。Firebaseでホスティングされており、Webブラウザでプレイできます。
キーボードのカーソルキーを使用して操作します。※スマートフォン、タブレット端末などでは動作しないかもしれません。

https://stardestroyergame.firebaseapp.com/

## Log

上記書籍は日記の体裁をとり、日々の記録をまとめるかたちで編集されましたが、下記のログは3日めで破綻しました。詳細に興味がある方は電子書籍をご覧ください。

### 2020.08.04 Remake your first game

- チュートリアルのゲームをリメイクしてみる
- プレイヤーの性能を変更する
- ボムの性能を変更する
- ボム同士が衝突するようにする


### 2020.08.03 Making your first Phaser 3 game

- 「Making your first Game」の後半
- 主に舞台にゲーム性を生み出す方法を学んだ
- キー入力を取得する機能を搭載している
- 行動によって得点が加算される
- 行動を進めるに連れ難易度が追加される
- 全てのステップを終え、ゲームが成立した


### 2020.08.02 Making your first Phaser 3 game

- 「Making your first Game」を開始した
- サンプルデータを取得してwebrootに設置した
- Google翻訳機能を使いながらドキュメントを読み進めた
- Part5までの手順を進めた



### 2020.08.01

- gitリポジトリを作成した
- yarnで最新版のPhaser3をインストールした
- yarnでhttp-serverをインストールした
- 公式ドキュメントより、Getting Started with Phaser 3を実行した
