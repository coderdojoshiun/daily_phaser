import Phaser from '../lib/phaser.js'

export default class GameStart extends Phaser.Scene
{
    constructor()
    {
        super('game-start')
    }

    preload()
    {
        this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js');

        this.load.audio('mission', 'assets/sfx/jingles_NES00.ogg')
    }

    create()
    {

        const width = this.scale.width
        const height = this.scale.height

        this.add.text(width * 0.5, height * 0.5, 'Game Start', {
            fontSize: 48
        })
        .setOrigin(0.5)

        this.customFont(this.add, 'スペースキーを押したら始まるんやで', width, height)

        this.input.keyboard.once('keydown_SPACE', () => {
            this.sound.play('mission')
            this.scene.start('game')
        })
    
    }

    customFont(target, text, width, height)
    {
        WebFont.load({
            custom: {
                families: ['jpnfont']
            },
            active: function(){
                target.text(width * 0.5, height * 0.75, text,
                {fontFamily: "jpnfont", fontSize: 12}).setOrigin(0.5)
            }
        })
    }
}
