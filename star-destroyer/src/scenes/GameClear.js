import Phaser from '../lib/phaser.js'

export default class GameClear extends Phaser.Scene
{
    constructor()
    {
        super('game-clear')
    }

    create()
    {
        this.sound.play('completed')

        const width = this.scale.width
        const height = this.scale.height

        this.add.text(width * 0.5, height * 0.5, 'Game Clear', {
            fontSize: 48
        })
        .setOrigin(0.5)

        this.customFont(this.add, '凄いやんけ！！ スペースキーを押したらまたできるで', width, height)


        this.input.keyboard.once('keydown_SPACE', () => {
            this.sound.play('mission')
            this.scene.start('game')
        })
    
    }


    customFont(target, text, width, height)
    {
        WebFont.load({
            custom: {
                families: ['jpnfont']
            },
            active: function(){
                target.text(width * 0.5, height * 0.75, text,
                {fontFamily: "jpnfont", fontSize: 12}).setOrigin(0.5)
            }
        })
    }
}
