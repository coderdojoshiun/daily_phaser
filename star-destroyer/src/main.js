import Phaser from './lib/phaser.js'

import Game from './scenes/Game.js'
import GameStart from './scenes/GameStart.js'
import GameOver from './scenes/GameOver.js'
import GameClear from './scenes/GameClear.js'

var config = {
    type: Phaser.AUTO,
    scale: {
        mode: Phaser.Scale.FIT,
        parent: 'phaser-example',
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: 832,
        height: 576
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    },
    scene: [GameStart, Game, GameOver, GameClear]
};


export default new Phaser.Game(config)