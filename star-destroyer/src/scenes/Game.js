import Phaser from '../lib/phaser.js'


export default class Game extends Phaser.Scene
{
    player

    stars

    bombs

    platforms

    cursors

    score = 0

    hiScore = 0

    gameOver = false

    scoreText

    hiScoreText

    hsv = []

    i = 0

    customPipeline

    sky

    t = 0

    timeEvent

    timerGraphics

    waveMode

    Fbutton

    countStars

    stage1

    constructor()
    {
        super('game')
    }

    init ()
    {
        var CustomPipeline2 = new Phaser.Class({

            Extends: Phaser.Renderer.WebGL.Pipelines.TextureTintPipeline,
        
            initialize:
        
            function CustomPipeline2 (game)
            {
                Phaser.Renderer.WebGL.Pipelines.TextureTintPipeline.call(this, {
                    game: game,
                    renderer: game.renderer,
                    fragShader: [
                    "precision mediump float;",
        
                    "uniform float     time;",
                    "uniform vec2      resolution;",
                    "uniform sampler2D uMainSampler;",
                    "varying vec2 outTexCoord;",
        
                    "void main( void ) {",
        
                        "vec2 uv = outTexCoord;",
                        "//uv.y *= -1.0;",
                        "uv.y += (sin((uv.x + (time * 0.5)) * 10.0) * 0.1) + (sin((uv.x + (time * 0.2)) * 32.0) * 0.01);",
                        "vec4 texColor = texture2D(uMainSampler, uv);",
                        "gl_FragColor = texColor;",
        
                    "}"
                    ].join('\n')
                });
            }
        
        });
        if (! this.customPipeline)
        {
            this.customPipeline = this.game.renderer.addPipeline('Custom', new CustomPipeline2(this.game));
            this.customPipeline.setFloat2('resolution', this.game.config.width, this.game.config.height);
        }

        this.score = 0

        this.timedEvent = this.time.delayedCall(60000, this.gameClear, [], this);

        this.countStars = 0
        this.waveMode = false

    }


    preload ()
    {
        this.load.image('sky', 'assets/backgroundCastles.png');
        this.load.image('ground', 'assets/platform.png');
        this.load.image('star', 'assets/star.png');
        this.load.image('bomb', 'assets/bomb.png');
        this.load.spritesheet('dude', 'assets/dude.png', { frameWidth: 32, frameHeight: 48 });
        this.load.spritesheet('fullscreen', 'assets/ui/fullscreen.png', { frameWidth: 64, frameHeight: 64 });

        //this.load.tilemapTiledJSON('stage1', 'assets/tilemap/star_destroyer_stage.json')
        this.load.tilemapCSV('stage1', 'assets/tilemap/star_destroyer_stage.csv')
        this.load.image('stageSheet', 'assets/tilemap/tilesheet_complete.png')

        this.load.audio('go', 'assets/sfx/go.ogg')
        this.load.audio('destroy', 'assets/sfx/war_target_destroyed.ogg')
        this.load.audio('completed', 'assets/sfx/mission_completed.ogg')
        this.load.audio('failed', 'assets/sfx/game_over.ogg')
        this.load.audio('newStars', 'assets/sfx/war_supressing_fire.ogg')
        this.load.audio('BGM', 'assets/sfx/strength-of-the-titans-by-kevin-macleod-from-filmmusic-io.mp3')

    }

    create ()
    {
        this.hsv = Phaser.Display.Color.HSVColorWheel();
        //this.customPipeline = this.game.renderer.addPipeline('Custom', new this.CustomPipeline2(this.game));
        //this.customPipeline.setFloat2('resolution', this.game.config.width, this.game.config.height);

        //  A simple background for our game
        this.sky = this.add.image(416, 288, 'sky');

        //  The platforms group contains the ground and the 2 ledges we can jump on
        this.platforms = this.physics.add.staticGroup();

        //  Here we create the ground.
        //  Scale it to fit the width of the game (the original sprite is 400x32 in size)
        //this.platforms.create(400, 568, 'ground').setScale(2).refreshBody();

        //  Now let's create some ledges
        this.platforms.create(600, 400, 'ground');
        //this.platforms.create(50, 250, 'ground');
        //this.platforms.create(750, 220, 'ground');

        const map = this.make.tilemap({key: 'stage1', tileWidth: 64, tileHeight: 64})
        const tileset = map.addTilesetImage('tilesheet_complete', 'stageSheet')
        this.stage1 = map.createStaticLayer('layer', tileset)
        map.setCollision([68, 95, 96, 200, 243, 244, 227, 228, 111, 112])

        // The player and its settings
        this.player = this.physics.add.sprite(300 , 250, 'dude');

        //  Player physics properties. Give the little guy a slight bounce.
        this.player.setBounce(0.2);
        this.player.setCollideWorldBounds(false);

        //  Our player animations, turning, walking left and walking right.
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'turn',
            frames: [ { key: 'dude', frame: 4 } ],
            frameRate: 20
        });

        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1
        });

        //  Input Events
        this.cursors = this.input.keyboard.createCursorKeys();

        var particles = this.add.particles('star');


        //  Some stars to collect, 12 in total, evenly spaced 70 pixels apart along the x axis
        this.stars = this.physics.add.group({
            key: 'star',
            repeat: 11,
            setXY: { x: 12, y: 0, stepX: 70 }
        });

        this.stars.children.iterate(function (child) {

            //  Give each star a slightly different bounce
            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
            child.setCollideWorldBounds(true);

            child.emitter = particles.createEmitter({
                speed: 100,
                scale: { start: 1, end: 0 },
                blendMode: 'ADD',
                frequency: 160
            });

            child.emitter.startFollow(child)

        });

        this.bombs = this.physics.add.group();

        //  The score
        this.scoreText = this.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#000' });
        this.hiScoreText = this.add.text(16, 40, 'Hi-Score: ' + this.hiScore, { fontSize: '24px', fill: '#000' });

        //  Collide the player and the stars with the platforms
        this.physics.add.collider(this.player, this.platforms);
        this.physics.add.collider(this.stars, this.platforms);
        this.physics.add.collider(this.bombs, this.platforms);
        
        this.physics.add.collider(this.player, this.stage1)
        this.physics.add.collider(this.stars, this.stage1)
        this.physics.add.collider(this.bombs, this.stage1)

        //  Checks to see if the player overlaps with any of the stars, if he does call the collectStar function
        this.physics.add.overlap(this.player, this.stars, this.collectStar, null, this);

        this.physics.add.overlap(this.bombs, this.bombs, this.deleteBomb, null, this);


        this.physics.add.collider(this.player, this.bombs, this.hitBomb, null, this);

        this.Fbutton = this.add.image(800-16, 16, 'fullscreen', 0).setOrigin(1, 0).setInteractive();

        this.Fbutton.on('pointerup', function () {

            if (this.scale.isFullscreen)
            {
                this.Fbutton.setFrame(0);

                this.scale.stopFullscreen();
            }
            else
            {
                this.Fbutton.setFrame(1);

                this.scale.startFullscreen();
            }

        }, this);

        var FKey = this.input.keyboard.addKey('F');

        FKey.on('down', function () {

            if (this.scale.isFullscreen)
            {
                this.Fbutton.setFrame(0);
                this.scale.stopFullscreen();
            }
            else
            {
                this.Fbutton.setFrame(1);
                this.scale.startFullscreen();
            }

        }, this);

        this.timerGraphics = this.add.graphics({x: 0, y: 0})

        this.sound.play('BGM')



    }

    update ()
    {
        //this.stars.setTint(0xFFFFFF << Math.random()*8092);

        if (this.gameOver)
        {
            this.gameOver = false

            this.setHiScore()
            this.sound.stopAll()

            this.scene.start('game-over')
        }

        if (this.cursors.left.isDown)
        {
            this.player.setVelocityX(-80);

            this.player.anims.play('left', true);
        }
        else if (this.cursors.right.isDown)
        {
            this.player.setVelocityX(80);

            this.player.anims.play('right', true);
        }
        else
        {
            this.player.setVelocityX(0);

            this.player.anims.play('turn');
        }

        if (this.cursors.up.isDown &&
             (this.player.body.touching.down || this.player.body.blocked.down) )
        {
            this.player.setVelocityY(-400);
            this.sound.play('go')
        }

        if (this.waveMode)
        {
            this.customPipeline.setFloat1('time', this.t);

            this.t += 0.005
        }


        this.horizontalWrap( this.player )

        this.timerGraphics.clear();

        this.drawClock(this.scale.width * 0.5, 48, this.timedEvent, this.timerGraphics)
    }

    deleteBomb (bomA, bomB)
    {
        bomB.disableBody(true, true);
    }

    collectStar (player, star)
    {
        star.emitter.stop()

        star.disableBody(true, true);

         this.sound.play('destroy')

        //  Add and update the score
        this.score += 10;
        this.scoreText.setText('Score: ' + this.score);

        if (this.stars.countActive(true) === 0)
        {
            //  A new batch of stars to collect
            this.stars.children.iterate(function (child) {

                child.enableBody(true, child.x, 0, true, true);
                child.emitter.start()

            });

            this.countStars++;

            this.sound.play('newStars')

            var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
            var bomb = this.bombs.create(x, 16, 'bomb');
            bomb.setBounce(Phaser.Math.FloatBetween(1, 1.1));
            bomb.setCollideWorldBounds(true);
            bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
            bomb.allowGravity = false;
            if(this.waveMode)
            {
                this.cameras.main.ignore([bomb]);
            }
            

        }
        if (!this.waveMode && (this.countStars > 1))
        {
            this.waveMode = true
            this.initWaveMode([ this.player, this.stars, this.platforms, this.bombs, this.stage1 ],
                [ this.sky, this.scoreText, this.hiScoreText, this.timerGraphics, this.Fbutton ])
        }
    }

    hitBomb (player, bomb)
    {
        this.physics.pause();

        player.setTint(0xff0000);

        player.anims.play('turn');

        this.gameOver = true;
    }

    /**
     * @param {Phaser.GameObjects.Sprite} sprite
     */
    horizontalWrap(sprite)
    {
        const halfWidth = sprite.displayWidth * 0.5
        const gameWidth = this.scale.width
        if (sprite.x < -halfWidth)
        {
            sprite.x = gameWidth + halfWidth
        }
        else if (sprite.x > gameWidth + halfWidth)
        {
            sprite.x = -halfWidth
        }
    }

    gameClear()
    {
        this.setHiScore()
        this.sound.stopAll()
        this.scene.start('game-clear')
    }


    //  https://phaser.io/examples/v3/view/time/time-scale
    drawClock (x, y, timer, graphics)
    {
        var clockSize = 40
        //  Progress is between 0 and 1, where 0 = the hand pointing up and then rotating clockwise a full 360

        //  The frame
        graphics.lineStyle(3, 0xffffff, 1);
        graphics.strokeCircle(x, y, clockSize);

        var angle;
        var dest;
        var p1;
        var p2;
        var size;

        //  The overall progress hand (only if repeat > 0)
        if (timer.repeat > 0)
        {
            size = clockSize * 0.9;

            angle = (360 * timer.getOverallProgress()) - 90;
            dest = Phaser.Math.RotateAroundDistance({ x: x, y: y }, x, y, Phaser.Math.DegToRad(angle), size);

            graphics.lineStyle(2, 0xff0000, 1);

            graphics.beginPath();

            graphics.moveTo(x, y);

            p1 = Phaser.Math.RotateAroundDistance({ x: x, y: y }, x, y, Phaser.Math.DegToRad(angle - 5), size * 0.7);

            graphics.lineTo(p1.x, p1.y);
            graphics.lineTo(dest.x, dest.y);

            graphics.moveTo(x, y);

            p2 = Phaser.Math.RotateAroundDistance({ x: x, y: y }, x, y, Phaser.Math.DegToRad(angle + 5), size * 0.7);

            graphics.lineTo(p2.x, p2.y);
            graphics.lineTo(dest.x, dest.y);

            graphics.strokePath();
            graphics.closePath();
        }

        //  The current iteration hand
        size = clockSize * 0.95;

        angle = (360 * timer.getProgress()) - 90;
        dest = Phaser.Math.RotateAroundDistance({ x: x, y: y }, x, y, Phaser.Math.DegToRad(angle), size);

        graphics.lineStyle(2, 0xffff00, 1);

        graphics.beginPath();

        graphics.moveTo(x, y);

        p1 = Phaser.Math.RotateAroundDistance({ x: x, y: y }, x, y, Phaser.Math.DegToRad(angle - 5), size * 0.7);

        graphics.lineTo(p1.x, p1.y);
        graphics.lineTo(dest.x, dest.y);

        graphics.moveTo(x, y);

        p2 = Phaser.Math.RotateAroundDistance({ x: x, y: y }, x, y, Phaser.Math.DegToRad(angle + 5), size * 0.7);

        graphics.lineTo(p2.x, p2.y);
        graphics.lineTo(dest.x, dest.y);

        graphics.strokePath();
        graphics.closePath();
    }

    initWaveMode(cam1Ignore, cam2Ignore)
    {
        this.cameras.main.ignore(cam1Ignore);

        var cam1 = this.cameras.main;
        var cam2 = this.cameras.add(0, 0, 832, 576);
        
        cam2.ignore(cam2Ignore);
        cam2.setRenderToTexture(this.customPipeline);
    }

    setHiScore()
    {
        if (this.hiScore < this.score)
        {
            this.hiScore = this.score
        }
        this.hiScore = this.score
    }


}
